#!/bin/ash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Change permission
chmod +x bin/cake

# Install git (the php image doesn't have it) which is required by composer
#apt-get update -yqq
#apt-get install git -yqq
apk update
apk add git

# Install icu 
apk add icu-dev

# Install unzip and zip
#apt-get install zip unzip
apk add zip unzip

# Install composer
curl -sS https://getcomposer.org/installer | php

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql
docker-php-ext-configure intl
docker-php-ext-install intl
apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug






